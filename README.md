# inception-elg

Build + install for Inception (see https://inception-project.github.io/downloads/)

Just drop the latest .jar in the repository for a new Build

/install folder contains scripts to start up the installation.

For /install:
* install docker if not present
* install mariadb
* run the db init scripts
* start up inception
