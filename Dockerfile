# This file is a template, and might need editing before it works on your project.
#FROM maven:3.5-jdk-11 as BUILD

FROM openjdk:11-jdk
#ENV PORT 4567

EXPOSE 8080

WORKDIR opt/inception/

COPY inception-app.standalone.jar /opt/inception/
RUN chmod +x /opt/inception/inception-app.standalone.jar

RUN mkdir /export
COPY settings.properties /export

CMD java -Dinception.home=/export -jar /opt/inception/inception-app.standalone.jar
#CMD [ "java",  "-Dinception.home=/export",  "-jar",  "/opt/inception/inception-app.standalone.jar ]
