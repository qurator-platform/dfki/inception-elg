#purge old mariadb install
sudo apt-get purge mariadb-server mariadb-* mysql-*

curl -LsS -O https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
sudo bash mariadb_repo_setup --mariadb-server-version=10.8.3

sudo apt update
sudo apt install mariadb-server mariadb-client
