#SET GLOBAL innodb_file_format = Barracuda;
#SET GLOBAL innodb_file_per_table = on;
#SET GLOBAL innodb_default_row_format = dynamic;
#SET GLOBAL innodb_large_prefix = 1;
#SET GLOBAL innodb_file_format_max = Barracuda;

CREATE DATABASE inception;
CREATE USER 'inception'@'localhost' IDENTIFIED BY 'inception';
GRANT ALL PRIVILEGES ON inception.* TO 'inception'@'localhost';
